// All the Setting related js functions will be here
$(document).ready(function () {
	// for get site url
	var url = $('.site_url').val();

	// icheck for the inputs
	$('.form').iCheck({
		checkboxClass: 'icheckbox_flat-green',
		radioClass: 'iradio_flat-green'
	});

	$('.flat_radio').iCheck({
		//checkboxClass: 'icheckbox_flat-green'
		radioClass: 'iradio_flat-green'
	});



	//for show rooms list
	 room_datatable = $('#rooms_table').DataTable({
		destroy: true,
		"processing": true,
		"serverSide": false,
		"ajax": url+"/room/room-list",
		"aoColumns": [
			{ mData: 'id'},
			{ mData: 'name' },
			{ mData: 'building_name' },
			{ mData: 'location_name'},
			{ mData: 'rate', className: "text-center"},
			{ mData: 'max_person', className: "text-center"},
			{ mData: 'status', className: "text-center"},
			{ mData: 'actions' , className: "text-center"},
		],
	});


	//Entry And Update Function For Module
	$("#save_room").on('click',function(){
		event.preventDefault();
		$.ajaxSetup({
			headers:{
				'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
			}
		});

		var formData = new FormData($('#room_form')[0]);

		if($.trim($('#name').val()) == ""){
			success_or_error_msg('#form_submit_error','danger',"Please Insert room Name","#name");
		}
		else if($.trim($('#building_id').val()) == 0){
			success_or_error_msg('#form_submit_error','danger',"Please Select a building","#building_id");
		}
		else if($.trim($('#price_per_hour').val()) == ""){
			success_or_error_msg('#form_submit_error','danger',"Please insert rate","#price_per_hour");
		}
		else{
			$.ajax({
				url: url+"/room/room-entry",
				type:'POST',
				data:formData,
				async:false,
				cache:false,
				contentType:false,
				processData:false,
				success: function(data){
					var response = JSON.parse(data);
					console.log(response['success']);
					if(!response['success']){
						var errors	= response['errors'];
						resultHtml = '<ul>';
							$.each(errors,function (k,v) {
							resultHtml += '<li>'+ v + '</li>';
						});
						resultHtml += '</ul>';
						success_or_error_msg('#master_message_div',"danger",resultHtml);
						clear_form();
					}
					else{
						success_or_error_msg('#master_message_div',"success","Save Successfully");
						room_datatable.ajax.reload();
						clear_form();
						$("#clear_button").show();
						$("#save_module").html('Save');
						$("#room_list_button").trigger('click');
						$("#save_room").html("save");
						$("#room_add_button").html('<b>Add Room</b>');
						$("#cancle_room_update").addClass('hidden');
						$("#room_image_add_button").addClass('hidden');
						$("#id").val('');
					}
					$(window).scrollTop();
				 }
			});
		}
		//console.log(formData);
	});


	//Clear form
	$("#clear_button").on('click',function(){
		clear_form();
	});


	//Edit function for Module
	roomEdit = function roomEdit(id){
		var edit_id = id;
		$.ajax({
			url: url+'/room/edit/'+edit_id,
			cache: false,
			success: function(response){
				var data = JSON.parse(response);
				$("#save_room").html('Update');
				$("#clear_button").hide();

				$("#room_add_button").trigger('click');
				$("#room_add_button").html('<b>Update Room</b>');
				$("#cancle_room_update").removeClass('hidden');
				$("#room_image_add_button").removeClass('hidden');
				$("#cancle_room_update").removeClass('hidden');

				$("#name").val(data['rooms']['name']);
				$("#building_id").val(data['rooms']['building_id']);
				$("#price_per_hour").val(data['rooms']['price_per_hour']);
				$("#maximum_person").val(data['rooms']['maximum_person']);
				$("#room_type").val(data['rooms']['room_type']);
				$("#working_hours_from").val(data['rooms']['working_hours_from']);
				$("#working_hours_till").val(data['rooms']['working_hours_till']);
				$("#details").val(data['rooms']['details']);

				$("#id").val(data['rooms']['id']);
				(data['rooms']['status']==0)?$("#status").iCheck('uncheck'):$("#status").iCheck('check');
			}
		});
	}


	//Edit function for Module
	roomAddImage = function roomAddImage(id){
		var edit_id = id;
		$.ajax({
			url: url+'/room/edit/'+edit_id,
			cache: false,
			success: function(response){
				var data = JSON.parse(response);
				$("#save_room").html('Update');
				$("#clear_button").hide();

				$("#room_image_add_button").trigger('click');
				$("#room_add_button").html('Update Room');
				$("#cancle_room_update").removeClass('hidden');
				$("#room_image_add_button").removeClass('hidden');
				$("#cancle_room_update").removeClass('hidden');

				$("#name").val(data['name']);
				$("#building_id").val(data['building_id']);
				$("#price_per_hour").val(data['price_per_hour']);
				$("#maximum_person").val(data['maximum_person']);
				$("#room_type").val(data['room_type']);
				$("#working_hours_from").val(data['working_hours_from']);
				$("#working_hours_till").val(data['working_hours_till']);
				$("#details").val(data['details']);

				(data['status']==0)?$("#status").iCheck('uncheck'):$("#status").iCheck('check');
			}
		});
	}


	//Edit function for Module
	roomView = function roomView(id){
		var edit_id = id;
		$.ajax({
			url: url+'/room/room-view/'+edit_id,
			cache: false,
			success: function(response){
				var data = JSON.parse(response);
				$("#save_room").html('Update');
				$("#clear_button").hide();

				$("#room_details_button").removeClass('hidden');
				$("#room_details_button").trigger('click');
				/*
				$("#room_image_add_button").trigger('click');
				$("#room_add_button").html('Update Room');
				$("#cancle_room_update").removeClass('hidden');
				$("#room_image_add_button").removeClass('hidden');
				$("#cancle_room_update").removeClass('hidden');

				$("#name").val(data['name']);
				$("#building_id").val(data['building_id']);
				$("#price_per_hour").val(data['price_per_hour']);
				$("#maximum_person").val(data['maximum_person']);
				$("#room_type").val(data['room_type']);
				$("#working_hours_from").val(data['working_hours_from']);
				$("#working_hours_till").val(data['working_hours_till']);
				$("#details").val(data['details']);
				*/
				(data['status']==0)?$("#status").iCheck('uncheck'):$("#status").iCheck('check');
			}
		});
	}

	$("#cancle_room_update").click(function(){
		clear_form();
		$(".save").html('Save');
		$("#room_list_button").trigger('click');
		$("#room_add_button").html('Add Room');
		$("#cancle_room_update").addClass('hidden');
		$("#clear_button").removeClass('hidden');
		$("#room_image_add_button").addClass('hidden');
		$("#id").val('');
	});


	//Delete Module
	roomDelete = function roomDelete(id){
		var delete_id = id;
		swal({
			title: "Are you sure?",
			text: "You wants to delete item parmanently!",
			icon: "warning",
			buttons: true,
			dangerMode: true,
		}).then((willDelete) => {
			if (willDelete) {
				$.ajax({
					url: url+'/room/delete/' + delete_id,
					cache: false,
					success: function(response){
						var response = JSON.parse(response);
						if (response['parentmessage']) {
							swal(response['parentmessage'], {
								icon: "warning",
							});
						}
						else{
							swal(response['deleteMessage'], {
								icon: "success",
							});
							room_datatable.ajax.reload();
						}
					}
				});
			}
			else {
				swal("Your Data is safe..!", {
				icon: "warning",
				});
			}
		});
	}
});

