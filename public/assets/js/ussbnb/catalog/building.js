// All the Setting related js functions will be here
$(document).ready(function () {
	// for get site url
	var url = $('.site_url').val();

	// icheck for the inputs
	$('.form').iCheck({
		checkboxClass: 'icheckbox_flat-green',
		radioClass: 'iradio_flat-green'
	});

	$('.flat_radio').iCheck({
		//checkboxClass: 'icheckbox_flat-green'
		radioClass: 'iradio_flat-green'
	});



	//for show buildings list
	 building_datatable = $('#buildings_table').DataTable({
		destroy: true,
		"processing": true,
		"serverSide": false,
		"ajax": url+"/building/building-list",
		"aoColumns": [
			{ mData: 'id'},
			{ mData: 'building_name' },
			{ mData: 'address' },
			{ mData: 'location_name'},
			{ mData: 'status', className: "text-center"},
			{ mData: 'actions' , className: "text-center"},
		],
	});




	/*
	//autosuggest
    $.ajaxSetup({
        headers:{
            'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
        }
    });
    $("#location_name").autocomplete({

        search: function() {
        },
        source: function(request, response) {
            $.ajax({
                url: url+'/location/location-list',
                dataType: "json",
                type: "post",
                async:false,
                data: {
                    term: request.term
                },
                success: function(data) {
                    response(data);
                }
            });
        },
        minLength: 1,
        select: function(event, ui) {
            var id = ui.item.id;
            var name = ui.item.name;
            $(this).next().val(id);
            $("#course_teacher").val(id);
        }
    });

*/







	//Entry And Update Function For Module
	$("#save_building").on('click',function(){
		event.preventDefault();
		$.ajaxSetup({
			headers:{
				'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
			}
		});

		var formData = new FormData($('#building_form')[0]);

		if($.trim($('#name').val()) == ""){
			success_or_error_msg('#form_submit_error','danger',"Please Insert building Name","#name");
		}
		else if($.trim($('#location_id').val()) == 0){
			success_or_error_msg('#form_submit_error','danger',"Please Select a location","#location_id");
		}
		else if($.trim($('#address').val()) == ""){
			success_or_error_msg('#form_submit_error','danger',"Please insert address","#address");
		}
		else{
			$.ajax({
				url: url+"/building/building-entry",
				type:'POST',
				data:formData,
				async:false,
				cache:false,
				contentType:false,
				processData:false,
				success: function(data){
					var response = JSON.parse(data);
					if(response['result'] == '0'){
						var errors	= response['errors'];
						resultHtml = '<ul>';
							$.each(errors,function (k,v) {
							resultHtml += '<li>'+ v + '</li>';
						});
						resultHtml += '</ul>';
						success_or_error_msg('#master_message_div',"danger",resultHtml);
						clear_form();
					}
					else{
						success_or_error_msg('#master_message_div',"success","Save Successfully");
						building_datatable.ajax.reload();
						clear_form();
						$("#clear_button").show();
						$("#save_module").html('Save');
						$("#building_list_button").trigger('click');
						$("#save_building").html("save");
						$("#building_add_button").html('Add Building');
						$("#cancle_building_update").addClass('hidden');
						$("#building_image_add_button").addClass('hidden');
						$("#id").val('');
					}
					$(window).scrollTop();
				 }
			});
		}
		//console.log(formData);
	});


	//Clear form
	$("#clear_button").on('click',function(){
		clear_form();
	});


	//Edit function for Module
	buildingEdit = function buildingEdit(id){
		var edit_id = id;
		$.ajax({
			url: url+'/building/edit/'+edit_id,
			cache: false,
			success: function(response){
				var data = JSON.parse(response);
				console.log(data);
				var res = data['data']['buildings'];
				$("#save_building").html('Update');
				$("#clear_button").hide();

				$("#building_add_button").trigger('click');
				$("#building_add_button").html('Update Building');
				$("#cancle_building_update").removeClass('hidden');
				$("#building_image_add_button").removeClass('hidden');
				$("#cancle_building_update").removeClass('hidden');
				$("#name").val(res['name']);
				$("#location_id").val(res['location_id']);
				$("#address").val(res['address']);
				$("#id").val(res['id']);
				(res['status']==0)?$("#status").iCheck('uncheck'):$("#status").iCheck('check');
			}
		});
	}



	//Edit function for Module
	buildingAddImage = function buildingAddImage(id){
		var edit_id = id;
		$.ajax({
			url: url+'/building/edit/'+edit_id,
			cache: false,
			success: function(response){
				var data = JSON.parse(response);
				$("#save_building").html('Update');
				$("#clear_button").hide();

				$("#building_image_add_button").trigger('click');
				$("#building_add_button").html('Update Building');
				$("#cancle_building_update").removeClass('hidden');
				$("#building_image_add_button").removeClass('hidden');
				$("#cancle_building_update").removeClass('hidden');
				$("#name").val(data['name']);
				$("#location_id").val(data['location_id']);
				$("#address").val(data['address']);
				$("#id").val(data['id']);
				(data['status']==0)?$("#status").iCheck('uncheck'):$("#status").iCheck('check');
			}
		});
	}


	$("#cancle_building_update").click(function(){
		clear_form();
		$(".save").html('Save');
		$("#building_list_button").trigger('click');
		$("#building_add_button").html('Add Building');
		$("#cancle_building_update").addClass('hidden');
		$("#clear_button").removeClass('hidden');
		$("#building_image_add_button").addClass('hidden');
		$("#id").val('');
	});


	//Delete Module
	buildingDelete = function buildingDelete(id){
		var delete_id = id;
		swal({
			title: "Are you sure?",
			text: "You wants to delete item parmanently!",
			icon: "warning",
			buttons: true,
			dangerMode: true,
		}).then((willDelete) => {
			if (willDelete) {
				$.ajax({
					url: url+'/building/delete/'+delete_id,
					cache: false,
					success: function(response){
						var response = JSON.parse(response);
						if (response['parentmessage']) {
							swal(response['parentmessage'], {
								icon: "warning",
							});
						}
						else{
							swal(response['deleteMessage'], {
								icon: "success",
							});
							building_datatable.ajax.reload();
						}
					}
				});
			}
			else {
				swal("Your Data is safe..!", {
				icon: "warning",
				});
			}
		});
	}
});

