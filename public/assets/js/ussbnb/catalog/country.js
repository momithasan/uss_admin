// All the user related js functions will be here
$(document).ready(function () {

    // for get site url
    var url = $('.site_url').val();

    /*App User Data Tables*/
    var country = $('#country').DataTable({
        destroy: true,
        "processing": true,
        "serverSide": false,
        "ajax": url+"/ajax/country/listing",
        "aoColumns": [

            { mData: 'id', className: "text-center"},
            { mData: 'name' },
            { mData: 'status', className: "text-center"},
            { mData: 'actions' , className: "text-center"},
        ],
    });

    /*-------- App Users Entry And Update Start --------*/
    $('#add_country').click(function(event){
        event.preventDefault();

        // $.ajaxSetup({
        //     headers:{
        //         'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
        //     }
        // });
        var formData = new FormData($('.countryForm')[0] );

        if($.trim($('#name').val()) == ""){
            success_or_error_msg('#form_submit_error','danger',"Please Insert Name","#name");
        }
        else{
            $.ajax({
                url: url+"/ajax/country/store",
                type:'POST',
                data:formData,
                async:false,
                cache:false,
                contentType:false,
                processData:false,
                success: function(data){
                    console.log(data);
                    var response = JSON.parse(data);
                    console.log(response);
                    success_or_error_msg('#master_message_div',"success",response['message']);

                    $(window).scrollTop();
                }
            });
        }
    });

    //Edit function for Module
    countryEdit = function countryEdit(id){
        var edit_id = id;
        $.ajax({
            url: url+'/ajax/country/edit/'+edit_id,
            cache: false,
            success: function(response){
                var data = JSON.parse(response);
                console.log(data);
                $("#add_country").html('Update');
                $("#clear_button").hide();

                $("#country_add_button").trigger('click');
                $("#country_add_button").html('<b>Update Country' +
                    '</b>');

                $("#name").val(data['data']['country'][0]['name']);
                $("#country_id").val(data['data']['country'][0]['id']);

                (data['status']==0)?$("#status").iCheck('uncheck'):$("#status").iCheck('check');
            }
        });
    }

    //Edit function for Module
    countryUpdate = function countryUpdate(id){
        var edit_id = id;
        $.ajax({
            url: url+'/ajax/country/edit/'+edit_id,
            cache: false,
            success: function(response){
                var data = JSON.parse(response);

                $("#add_country").html('Update');
                $("#clear_button").hide();

                $("#country_add_button").trigger('click');
                $("#country_add_button").html('<b>Update Room</b>');

                $("#name").val(data['data']['country'][0]['name']);

                (data['status']==0)?$("#status").iCheck('uncheck'):$("#status").iCheck('check');
            }
        });
    }

    //Delete Module
    countryDelete = function countryDelete(id){
        var delete_id = id;
        swal({
            title: "Are you sure?",
            text: "You wants to delete item parmanently!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    url: url+'/ajax/country/delete/' + delete_id,
                    cache: false,
                    success: function(response){
                        var response = JSON.parse(response);
                        console.log(response);

                        if (response['parentmessage']) {
                            swal(response['parentmessage'], {
                                icon: "warning",
                            });
                        }
                        else{
                            swal(response['deleteMessage'], {
                                icon: "success",
                            });
                            country.ajax.reload();
                        }
                    }
                });
            }
            else {
                swal("Your Data is safe..!", {
                    icon: "warning",
                });
            }
        });
    }

    /*-------- App Users Entry And Update End --------*/
});





