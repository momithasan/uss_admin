$(document).ready(function () {

    // for get site url
    var url = $('.site_url').val();

    /*App User Data Tables*/
    var building = $('#building').DataTable({
        destroy: true,
        "processing": true,
        "serverSide": false,
        "ajax": url+"/ajax/building/listing",
        "aoColumns": [

            { mData: 'id', className: "text-center"},
            { mData: 'name' },
            { mData: 'address' },
            { mData: 'location' },
            { mData: 'status', className: "text-center"},
            { mData: 'actions' , className: "text-center"},
        ],
    });

    /*-------- App Users Entry And Update Start --------*/
    $('#add_country').click(function(event){
        event.preventDefault();

        // $.ajaxSetup({
        //     headers:{
        //         'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
        //     }
        // });
        var formData = new FormData($('.countryForm')[0] );

        if($.trim($('#name').val()) == ""){
            success_or_error_msg('#form_submit_error','danger',"Please Insert Name","#name");
        }
        else{
            $.ajax({
                url: url+"/ajax/country/store",
                type:'POST',
                data:formData,
                async:false,
                cache:false,
                contentType:false,
                processData:false,
                success: function(data){
                    console.log(data);
                    var response = JSON.parse(data);
                    console.log(response['message']);
                    success_or_error_msg('#master_message_div',"success",response['message']);
                    // if(response['message']){
                    //     var errors	= response['errors'];
                    //     resultHtml = '<ul>';
                    //     $.each(errors,function (k,v) {
                    //         resultHtml += '<li>'+ v + '</li>';
                    //     });
                    //     resultHtml += '</ul>';
                    //     success_or_error_msg('#master_message_div',"danger",resultHtml);
                    // }
                    // else{
                    //
                    //
                    //     $("#country_add").trigger('click');
                    //     country.ajax.reload();
                    //
                    //     clear_form();
                    //     $("#app_user_button").html('Add Country');
                    //     $("#save_app_user_info").html('Save');
                    //     $("#cancle_app_user_update").addClass('hide');
                    //     $("#app_user_edit_id").val('');
                    //     $("#app_user_img").attr("src", "src");
                    //     country();
                    // }
                    $(window).scrollTop();
                }
            });
        }
    });

    /*-------- App Users Entry And Update Start --------*/
    $('#update_country').click(function(event){
        event.preventDefault();
        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
            }
        });
        var formData = new FormData($('.countryForm')[0] );
        dd()
        if($.trim($('#name').val()) == ""){
            success_or_error_msg('#form_submit_error','danger',"Please Insert Name","#name");
        }
        else{
            $.ajax({
                url: url+"/ajax/country/update/"+formData.get('id'),
                type:'PUT',
                data:formData,
                async:false,
                cache:false,
                contentType:false,
                processData:false,
                success: function(data){
                    console.log(data);
                    var response = JSON.parse(data);
                    console.log(response['message']);
                    success_or_error_msg('#master_message_div',"success",response['message']);
                    // if(response['message']){
                    //     var errors	= response['errors'];
                    //     resultHtml = '<ul>';
                    //     $.each(errors,function (k,v) {
                    //         resultHtml += '<li>'+ v + '</li>';
                    //     });
                    //     resultHtml += '</ul>';
                    //     success_or_error_msg('#master_message_div',"danger",resultHtml);
                    // }
                    // else{
                    //
                    //
                    //     $("#country_add").trigger('click');
                    //     country.ajax.reload();
                    //
                    //     clear_form();
                    //     $("#app_user_button").html('Add Country');
                    //     $("#save_app_user_info").html('Save');
                    //     $("#cancle_app_user_update").addClass('hide');
                    //     $("#app_user_edit_id").val('');
                    //     $("#app_user_img").attr("src", "src");
                    //     country();
                    // }
                    $(window).scrollTop();
                }
            });
        }
    });

    /*-------- App Users Entry And Update End --------*/
});





