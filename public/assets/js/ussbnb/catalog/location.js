// All the Setting related js functions will be here
$(document).ready(function () {
	// for get site url
	var url = $('.site_url').val();

	// icheck for the inputs
	$('.form').iCheck({
		checkboxClass: 'icheckbox_flat-green',
		radioClass: 'iradio_flat-green'
	});

	$('.flat_radio').iCheck({
		//checkboxClass: 'icheckbox_flat-green'
		radioClass: 'iradio_flat-green'
	});



	//for show locations list
	 location_datatable = $('#locations_table').DataTable({
		destroy: true,
		"processing": true,
		"serverSide": false,
		"ajax": url+"/locations/location-list",
		"aoColumns": [
			{ mData: 'id'},
			{ mData: 'location_name' },
			{ mData: 'country_name'},
			{ mData: 'status', className: "text-center"},
			{ mData: 'actions' , className: "text-center"},
		],
	});


	//dynamic dropdown
	load_country = function load_country(){
	//Getting Parrent Menu
		$.ajax({
			url : url+"/country/country-list",
			cache: false,
			dataType: 'json',
			success: function(response){
				//console.log(response);
				var data = response.data;
				var option = '';
				$.each(data,function(index,row){
					option +="<option>";
					option += row.name;
					option += "</option>";
				})
				//	alert(option)
				$("#country_list").html(option);
				//console.log(data[0]['module_name']);
			}
		});
	}
	load_country();


	/*
	//autosuggest
    $.ajaxSetup({
        headers:{
            'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
        }
    });
    $("#country_name").autocomplete({

        search: function() {
        },
        source: function(request, response) {
            $.ajax({
                url: url+'/country/country-list',
                dataType: "json",
                type: "post",
                async:false,
                data: {
                    term: request.term
                },
                success: function(data) {
                    response(data);
                }
            });
        },
        minLength: 1,
        select: function(event, ui) {
            var id = ui.item.id;
            var name = ui.item.name;
            $(this).next().val(id);
            $("#course_teacher").val(id);
        }
    });

*/







	//Entry And Update Function For Module
	$("#save_location").on('click',function(){
		event.preventDefault();
		$.ajaxSetup({
			headers:{
				'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
			}
		});

		var formData = new FormData($('#location_form')[0]);

		if($.trim($('#location_name').val()) == ""){
			success_or_error_msg('#form_submit_error','danger',"Please Insert Location Name","#location_name");
		}
		else if($.trim($('#country_id').val()) == ""){
			success_or_error_msg('#form_submit_error','danger',"Please Select a country","#country_id");
		}
		else{
			$.ajax({
				url: url+"/location/location-entry",
				type:'POST',
				data:formData,
				async:false,
				cache:false,
				contentType:false,
				processData:false,
				success: function(data){
					var response = JSON.parse(data);
					if(response['result'] == '0'){
						var errors	= response['errors'];
						resultHtml = '<ul>';
							$.each(errors,function (k,v) {
							resultHtml += '<li>'+ v + '</li>';
						});
						resultHtml += '</ul>';
						success_or_error_msg('#master_message_div',"danger",resultHtml);
						clear_form();
					}
					else{
						success_or_error_msg('#master_message_div',"success","Save Successfully");
						location_datatable.ajax.reload();
						load_country();
						clear_form();
						$("#clear_button").show();
						$("#save_module").html('Save');
					}
					$(window).scrollTop();
				 }
			});
		}
		//console.log(formData);
	});


	//Clear form
	$("#clear_button").on('click',function(){
		clear_form();
	});


	//Edit function for Module
	locationEdit = function locationEdit(id){
		var edit_id = id;
		$.ajax({
			url: url+'/location/edit/'+edit_id,
			cache: false,
			success: function(response){
				var data = JSON.parse(response);
				$("#save_location").html('Update');
				$("#clear_button").hide();
				$("#location_name").val(data['locations']['location_name']);
				$("#country_id").val(data['locations']['country_id']);
				$("#edit_id").val(edit_id);
				(data['status']==0)?$("#status").iCheck('uncheck'):$("#status").iCheck('check');
			}
		});
	}


	//Delete Module
	locationDelete = function locationDelete(id){
		var delete_id = id;
		swal({
			title: "Are you sure?",
			text: "You wants to delete item parmanently!",
			icon: "warning",
			buttons: true,
			dangerMode: true,
		}).then((willDelete) => {
			if (willDelete) {
				$.ajax({
					url: url+'/location/delete/'+delete_id,
					cache: false,
					success: function(response){
						var response = JSON.parse(response);
						if (response['parentmessage']) {
							swal(response['parentmessage'], {
								icon: "warning",
							});
						}
						else{
							swal(response['deleteMessage'], {
								icon: "success",
							});
							location_datatable.ajax.reload();
						}
					}
				});
			}
			else {
				swal("Your Data is safe..!", {
				icon: "warning",
				});
			}
		});
	}
});

