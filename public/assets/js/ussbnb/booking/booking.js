// All the Setting related js functions will be here
$(document).ready(function () {

	// for get site url
	var url = $('.site_url').val();

	// icheck for the inputs
	$('.form').iCheck({
		checkboxClass: 'icheckbox_flat-green',
		radioClass: 'iradio_flat-green'
	});

	$('.flat_radio').iCheck({
		//checkboxClass: 'icheckbox_flat-green'
		radioClass: 'iradio_flat-green'
	});

    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
    });

	//for show bookings list
	 booking_datatable = $('#bookings_table').DataTable({
		destroy: true,
		"processing": true,
		"serverSide": false,
		"ajax": url+"/booking/booking-list",
		"aoColumns": [
			{ mData: 'id'},
			{ mData: 'client_name' },
			{ mData: 'guest_name' },
			{ mData: 'room_details'},
			{ mData: 'from', className: "text-center"},
			{ mData: 'till', className: "text-center"},
			{ mData: 'price', className: "text-right"},
			{ mData: 'status', className: "text-center"},
			{ mData: 'actions' , className: "text-center"},
		],
	});


	/*
	//autosuggest
=======
    // for get site url
    var url = $('.site_url').val();

    // icheck for the inputs
    $('.form').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
    });

    $('.flat_radio').iCheck({
        //checkboxClass: 'icheckbox_flat-green'
        radioClass: 'iradio_flat-green'
    });

    // $("#company_data").hide()

    //for show bookings list
    booking_datatable = $('#bookings_table').DataTable({
        destroy: true,
        "processing": true,
        "serverSide": false,
        "ajax": url + "/booking/booking-list",
        "aoColumns": [
            {mData: 'id'},
            {mData: 'client_name'},
            {mData: 'guest_name'},
            {mData: 'room_details'},
            {mData: 'from', className: "text-center"},
            {mData: 'till', className: "text-center"},
            {mData: 'price', className: "text-right"},
            {mData: 'status', className: "text-center"},
            {mData: 'actions', className: "text-center"},
        ],
    });


    /*
    //autosuggest
>>>>>>> Stashed changes
    $.ajaxSetup({
        headers:{
            'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
        }
    });
    $("#location_name").autocomplete({

        search: function() {
        },
        source: function(request, response) {
            $.ajax({
                url: url+'/location/location-list',
                dataType: "json",
                type: "post",
                async:false,
                data: {
                    term: request.term
                },
                success: function(data) {
                    response(data);
                }
            });
        },
        minLength: 1,
        select: function(event, ui) {
            var id = ui.item.id;
            var name = ui.item.name;
            $(this).next().val(id);
            $("#course_teacher").val(id);
        }
    });

*/


	// //Entry And Update Function For Module
	// $("#save_booking").on('click',function(){
	// 	event.preventDefault();
	// 	$.ajaxSetup({
	// 		headers:{
	// 			'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
	// 		}
	// 	});
    //
	// 	var formData = new FormData($('#booking_form')[0]);
    //
	// 	if($.trim($('#name').val()) == ""){
	// 		success_or_error_msg('#form_submit_error','danger',"Please Insert booking Name","#name");
	// 	}
	// 	else if($.trim($('#building_id').val()) == 0){
	// 		success_or_error_msg('#form_submit_error','danger',"Please Select a building","#building_id");
	// 	}
	// 	else if($.trim($('#price_per_hour').val()) == ""){
	// 		success_or_error_msg('#form_submit_error','danger',"Please insert rate","#price_per_hour");
	// 	}
	// 	else{
	// 		$.ajax({
	// 			url: url+"/booking/booking-entry",
	// 			type:'POST',
	// 			data:formData,
	// 			async:false,
	// 			cache:false,
	// 			contentType:false,
	// 			processData:false,
	// 			success: function(data){
	// 				var response = JSON.parse(data);
	// 				if(response['result'] == '0'){
	// 					var errors	= response['errors'];
	// 					resultHtml = '<ul>';
	// 						$.each(errors,function (k,v) {
	// 						resultHtml += '<li>'+ v + '</li>';
	// 					});
	// 					resultHtml += '</ul>';
	// 					success_or_error_msg('#master_message_div',"danger",resultHtml);
	// 					clear_form();
	// 				}
	// 				else{
	// 					success_or_error_msg('#master_message_div',"success","Save Successfully");
	// 					booking_datatable.ajax.reload();
	// 					clear_form();
	// 					$("#clear_button").show();
	// 					$("#save_module").html('Save');
	// 					$("#booking_list_button").trigger('click');
	// 					$("#save_booking").html("save");
	// 					$("#booking_add_button").html('<b>Add Booking</b>');
	// 					$("#cancle_booking_update").addClass('hidden');
	// 					$("#booking_image_add_button").addClass('hidden');
	// 					$("#id").val('');
	// 				}
	// 				$(window).scrollTop();
	// 			 }
	// 		});
	// 	}
	// 	//console.log(formData);
	// });
    //
    //
	// //Clear form
	// $("#clear_button").on('click',function(){
	// 	clear_form();
	// });
    //
    //
	// //Edit function for Module
	// bookingEdit = function bookingEdit(id){
	// 	var edit_id = id;
	// 	$.ajax({
	// 		url: url+'/booking/edit/'+edit_id,
	// 		cache: false,
	// 		success: function(response){
	// 			var data = JSON.parse(response);
	// 			$("#save_booking").html('Update');
	// 			$("#clear_button").hide();
    //
	// 			$("#booking_add_button").trigger('click');
	// 			$("#booking_add_button").html('<b>Update Booking</b>');
	// 			$("#cancle_booking_update").removeClass('hidden');
	// 			$("#booking_image_add_button").removeClass('hidden');
	// 			$("#cancle_booking_update").removeClass('hidden');
    //
	// 			$("#name").val(data['name']);
	// 			$("#building_id").val(data['building_id']);
	// 			$("#price_per_hour").val(data['price_per_hour']);
	// 			$("#maximum_person").val(data['maximum_person']);
	// 			$("#booking_type").val(data['booking_type']);
	// 			$("#working_hours_from").val(data['working_hours_from']);
	// 			$("#working_hours_till").val(data['working_hours_till']);
	// 			$("#details").val(data['details']);
    //
	// 			$("#id").val(data['id']);
	// 			(data['status']==0)?$("#status").iCheck('uncheck'):$("#status").iCheck('check');
	// 		}
	// 	});
	// }
    //
    //
    //
	// //Edit function for Module
	// bookingView = function bookingView(id){
	// 	var edit_id = id;
	// 	alert(id)
	// 	$.ajax({
	// 		url: url+'/booking/booking-view/'+edit_id,
	// 		cache: false,
	// 		success: function(response){
	// 			var data = JSON.parse(response);
	// 			$("#save_booking").html('Update');
	// 			$("#clear_button").hide();
    //
	// 			$("#booking_details_button").removeClass('hidden');
	// 			$("#booking_details_button").trigger('click');
    //
    //
	// 			$("#booking_add_button").html('Update Booking');
	// 			$("#cancle_booking_update").removeClass('hidden');
    //
    //
	// 			(data['status']==0)?$("#status").iCheck('uncheck'):$("#status").iCheck('check');
	// 		}
	// 	});
	// }
    //
	// $("#cancle_booking_update").click(function(){
	// 	clear_form();
	// 	$(".save").html('Save');
	// 	$("#booking_list_button").trigger('click');
	// 	$("#booking_add_button").html('Add Booking');
	// 	$("#cancle_booking_update").addClass('hidden');
	// 	$("#clear_button").removeClass('hidden');
	// 	$("#booking_image_add_button").addClass('hidden');
	// 	$("#id").val('');
	// });
    //
    //
	// //Delete Module
	// bookingDelete = function bookingDelete(id){
	// 	var delete_id = id;
	// 	swal({
	// 		title: "Are you sure?",
	// 		text: "You wants to delete item parmanently!",
	// 		icon: "warning",
	// 		buttons: true,
	// 		dangerMode: true,
	// 	}).then((willDelete) => {
	// 		if (willDelete) {
	// 			$.ajax({
	// 				url: url+'/booking/delete/'+delete_id,
	// 				cache: false,
	// 				success: function(response){
	// 					var response = JSON.parse(response);
	// 					if (response['parentmessage']) {
	// 						swal(response['parentmessage'], {
	// 							icon: "warning",
	// 						});
	// 					}
	// 					else{
	// 						swal(response['deleteMessage'], {
	// 							icon: "success",
	// 						});
	// 						booking_datatable.ajax.reload();
	// 					}
	// 				}
	// 			});
	// 		}
	// 		else {
	// 			swal("Your Data is safe..!", {
	// 			icon: "warning",
	// 			});
	// 		}
	// 	});
	// }

    //Entry And Update Function For Module
    $("#save_booking").on('click', function () {
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var formData = new FormData($('#booking_form')[0]);


        $.ajax({
            url: url + "/booking/booking-entry",
            type: 'POST',
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                var response = JSON.parse(data);
                if (response['result'] == '0') {
                    var errors = response['errors'];
                    resultHtml = '<ul>';
                    $.each(errors, function (k, v) {
                        resultHtml += '<li>' + v + '</li>';
                    });
                    resultHtml += '</ul>';
                    success_or_error_msg('#master_message_div', "danger", resultHtml);
                    clear_form();
                } else {
                    console.log()
                    success_or_error_msg('#master_message_div', "success", "Save Successfully");
                    booking_datatable.ajax.reload();
                    clear_form();
                    $("#clear_button").show();
                    $("#save_module").html('Save');
                    $("#booking_list_button").trigger('click');
                    $("#save_booking").html("save");
                    $("#booking_add_button").html('<b>Add Booking</b>');
                    $("#cancle_booking_update").addClass('hidden');
                    $("#booking_image_add_button").addClass('hidden');
                    $("#id").val('');
                }
                $(window).scrollTop();
            }
        });
    });


    //Clear form
    $("#clear_button").on('click', function () {
        clear_form();
    });


    //Edit function for Module
    bookingEdit = function bookingEdit(id) {
        var edit_id = id;
        $.ajax({
            url: url + '/booking/edit/' + edit_id,
            cache: false,
            success: function (response) {
                var data = JSON.parse(response);
                console.log(data);
                $("#save_booking").html('Update');
                $("#clear_button").hide();
                $("#save_booking").removeClass('save');
                $("#save_booking").addClass('update');
                $("#booking_add_button").trigger('click');
                $("#booking_add_button").html('<b>Update Booking</b>');
                $("#cancle_booking_update").removeClass('hidden');
                $("#booking_image_add_button").removeClass('hidden');
                $("#cancle_booking_update").removeClass('hidden');

                $("#booking_id").val(data['booking']['id']);
                $("#seat_number").val(data['booking']['seats']);
                $("#comment").val(data['booking']['comment']);
                $("#price").val(data['booking']['price']);
                $("#guest_first_name").val(data['booking']['guest_first_name']);
                $("#guest_last_name").val(data['booking']['guest_last_name']);
                $("#booking_from").val(data['booking']['from']);
                $("#start_time").val(data['booking']['start_time']);
                $("#booking_till").val(data['booking']['till']);
                $("#end_time").val(data['booking']['end_time']);
                $("#guest_email").val(data['booking']['guest_email']);
                $("#guest_company_contacts").val(data['booking']['guest_company_contacts']);
                $("#guest_company_name").val(data['booking']['guest_company_name']);
                $("#guest_company_reg_nr").val(data['booking']['guest_company_reg_nr']);
                $("#guest_company_vat").val(data['booking']['guest_company_vat']);
                $("#guest_company_address").val(data['booking']['guest_company_address']);
                $("#guest_id").val(data['booking']['guest_id']);

                //$("#id").val(data['id']);
                (data['booking']['booking_status_id'] == 0) ? $("#status").iCheck('uncheck') : $("#booking_status").iCheck('check');
            }
        });
    }


    //Edit function for Module
    bookingAddImage = function bookingAddImage(id) {
        var edit_id = id;
        $.ajax({
            url: url + '/booking/edit/' + edit_id,
            cache: false,
            success: function (response) {
                var data = JSON.parse(response);
                $("#save_booking").html('Update');
                $("#clear_button").hide();

                $("#booking_image_add_button").trigger('click');
                $("#booking_add_button").html('Update Booking');
                $("#cancle_booking_update").removeClass('hidden');
                $("#booking_image_add_button").removeClass('hidden');
                $("#cancle_booking_update").removeClass('hidden');

                $("#name").val(data['name']);
                $("#building_id").val(data['building_id']);
                $("#price_per_hour").val(data['price_per_hour']);
                $("#maximum_person").val(data['maximum_person']);
                $("#booking_type").val(data['booking_type']);
                $("#working_hours_from").val(data['working_hours_from']);
                $("#working_hours_till").val(data['working_hours_till']);
                $("#details").val(data['details']);

                (data['status'] == 0) ? $("#status").iCheck('uncheck') : $("#status").iCheck('check');
            }
        });
    }


    //Edit function for Module
    bookingView = function bookingView(id) {
        var edit_id = id;
        $.ajax({
            url: url + '/booking/view/' + edit_id,
            cache: false,
            success: function (response) {
                var data = JSON.parse(response);
                console.log(data);
                $("#save_booking").html('Update');
                $("#clear_button").hide();

                $("#booking_details_button").removeClass('hidden');
                $("#booking_details_button").trigger('click');
                $("#guest_name").html(data['guest_first_name'] +' '+data['guest_last_name']);
                $("#guest_email2").html(data['guest_email']);
                $("#guest_phone2").html(data['guest_company_contacts']);
                $("#booking_from2").html(data['from']);
                $("#booking_till2").html(data['till']);
                $("#price2").html(data['price']);
                $("#seats2").html(data['seats']);
                $("#comments2").html(data['comments']);
                $("#room2").html(data['room']["data"]["name"]+", "+data["room"]["data"]["buildings"][0]["name"] );



                (data['status'] == 0) ? $("#status").iCheck('uncheck') : $("#status").iCheck('check');
            }
        });
    }

    $("#cancle_booking_update").click(function () {
        clear_form();
        $(".save").html('Save');
        $("#booking_list_button").trigger('click');
        $("#booking_add_button").html('Add Booking');
        $("#cancle_booking_update").addClass('hidden');
        $("#clear_button").removeClass('hidden');
        $("#booking_image_add_button").addClass('hidden');
        $("#id").val('');
    });


    //Delete Module
    bookingDelete = function bookingDelete(id) {
        var delete_id = id;
        swal({
            title: "Are you sure?",
            text: "You wants to delete item parmanently!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    url: url + '/booking/delete/' + delete_id,
                    cache: false,
                    success: function (response) {
                        var response = JSON.parse(response);
                        if (response['parentmessage']) {
                            swal(response['parentmessage'], {
                                icon: "warning",
                            });
                        } else {
                            swal(response['deleteMessage'], {
                                icon: "success",
                            });
                            booking_datatable.ajax.reload();
                        }
                    }
                });
            } else {
                swal("Your Data is safe..!", {
                    icon: "warning",
                });
            }
        });
    }
    bookingStauts = function bookingStauts(status, id) {
        event.preventDefault();


        $.ajax({
            url: url + "/booking/status/" + status + "/" + id,
            type: 'GET',
            success: function (data) {
                var response = JSON.parse(data);
                if (response['code'] == '500') {
                    var errors = response['errors'];
                    resultHtml = '<ul>';
                    $.each(errors, function (k, v) {
                        resultHtml += '<li>' + v + '</li>';
                    });
                    resultHtml += '</ul>';
                    success_or_error_msg('#master_message_div', "danger", resultHtml);
                    clear_form();
                } else {
                    console.log(response)
                    success_or_error_msg('#master_message_div', "success", "Save Successfully");
                    booking_datatable.ajax.reload();
                    clear_form();
                }
                $(window).scrollTop();
            }
        });
    }

    // bookingStauts = function bookingDelete(id) {
    //     event.preventDefault();
    //
    //     $.ajax({
    //         url: url + "/booking/delete" + "/" + id,
    //         type: 'GET',
    //         success: function (data) {
    //             console.log(data);
    //             var response = JSON.parse(data);
    //             if (response['code'] == '500') {
    //                 var errors = response['errors'];
    //                 resultHtml = '<ul>';
    //                 $.each(errors, function (k, v) {
    //                     resultHtml += '<li>' + v + '</li>';
    //                 });
    //                 resultHtml += '</ul>';
    //                 success_or_error_msg('#master_message_div', "danger", resultHtml);
    //                 clear_form();
    //             } else {
    //                 console.log(response)
    //                 success_or_error_msg('#master_message_div', "danger", "Deleted Successfully");
    //                 booking_datatable.ajax.reload();
    //                 clear_form();
    //             }
    //             $(window).scrollTop();
    //         }
    //     });
    // }


});

function isCompany() {
    alert();
    if ($(this).is(":checked"))
        $('#company_data').show('400');
    else
        $('#company_data').hide('200');

}
