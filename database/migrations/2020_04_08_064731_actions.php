<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Actions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       	Schema::create('actions', function (Blueprint $table) {
			$table->id();
			$table->string('activity_name');
			$table->unsignedBigInteger('module_id');
			$table->unsignedBigInteger('is_menu')->nullable()->comment('if the action related with a menu/submenu link then select the menu'); 
			$table->tinyInteger('status')->comment('1:active,0:in-active')->default('1'); 
			$table->foreign('module_id')->references('id')->on('menus');
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
