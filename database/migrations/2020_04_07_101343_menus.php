<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Menus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('menus', function (Blueprint $table) {
			$table->id();
			$table->string('module_name');
			$table->string('menu_title');
			$table->string('menu_url');
			$table->integer('parent_id')->comment('value:0 if the menu is itself a parent otherwise anyother parent id')default('0'); 
			$table->integer('serial_no')->nullable(); 
			$table->string('menu_icon_class')->nullable(); 
			$table->tinyInteger('status')->comment('1:active,0:in-active')->default('1'); 
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
} 
