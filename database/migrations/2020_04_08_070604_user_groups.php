<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('user_groups', function (Blueprint $table) {
			$table->id();
			$table->string('group_name');
			$table->tinyInteger('type')->comment('1: Admin User, 2: client User')->default('1'); 
			$table->tinyInteger('status')->comment('1:active,0:in-active')->default('1'); 
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
