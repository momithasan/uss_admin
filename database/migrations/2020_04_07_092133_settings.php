<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Settings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('settings', function (Blueprint $table) {
            $table->id();
            $table->string('company_name');
			$table->string('short_name');
			$table->string('site_name');
			$table->string('admin_email');
			$table->string('admin_mobile'); 
			$table->string('site_url'); 
			$table->string('admin_url'); 
			$table->string('logo'); 
			$table->string('address'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
