<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Adminuser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
			$table->string('last_name')->nullable();
            $table->string('email')->unique();
			$table->string('user_profile_image')->nullable();
			$table->string('contact_no')->nullable(); 
			$table->text('remarks')->nullable();
			$table->integer('client_id')->nullable()->comment('when a user is under a client then client id will set here'); 
			$table->tinyInteger('status')->comment('1:active,0:in-active')->default('1'); 
			$table->tinyInteger('login_status')->comment('1:logged-in,0:Not logged in')->default('0'); 
			$table->string('password');
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
