<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserGroupPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('user_group_permissions', function (Blueprint $table) {
			$table->id();
			$table->unsignedBigInteger('group_id');
			$table->unsignedBigInteger('action_id');
			$table->tinyInteger('status')->comment('1:active,0:in-active')->default('1'); 
			$table->timestamps();
			$table->foreign('group_id')->references('id')->on('user_groups');
			$table->foreign('action_id')->references('id')->on('actions');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
