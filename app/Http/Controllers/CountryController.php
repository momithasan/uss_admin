<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\CatalogController;
use App\Traits\HasPermission;
use Auth;
class CountryController extends Controller
{
    //
    use HasPermission;
    public $catalogObj;
    public function __construct(Request $request)
    {
        $this->page_title = $request->route()->getName();
        $description = \Request::route()->getAction();
        $this->page_desc = isset($description['desc']) ? $description['desc'] : $this->page_title;
    }



    public function ajaxCountryList(){
        $admin_user_id 		= Auth::user()->id;
        $edit_action_id 	= 10;
        $delete_action_id 	= 11;
        $edit_permisiion 	= $this->PermissionHasOrNot($admin_user_id,$edit_action_id);
        $delete_permisiion 	= $this->PermissionHasOrNot($admin_user_id,$delete_action_id);

        $countries = $this->catalogObj->getCountries();

        $return_arr = array();
        foreach($countries as $country){
            $country['status']=($country->status == 1)?"<button class='btn btn-xs btn-success' disabled>Active</button>":"<button class='btn btn-xs btn-success' disabled>In-active</button>";
            $country['actions'] = "";
            $country['name'] = $country->name;
            if($edit_permisiion>0){
                $country['actions'] .="<button onclick='moduleEdit(".$country->id.")' id=edit_" . $country->id . "  class='btn btn-xs btn-green module-edit' ><i class='clip-pencil-3'></i></button>";
            }
            if ($delete_permisiion>0) {
                $country['actions'] .=" <button onclick='moduleDelete(".$country->id.")' id='delete_" . $country->id . "' class='btn btn-xs btn-danger' ><i class='clip-remove'></i></button>";
            }
            $return_arr[] = $country;
        }
        return json_encode(array('data'=>$return_arr));
    }
}
