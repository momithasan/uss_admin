<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
//use App\Services\USSBNBMicroService;
use App\Services\UssbnbService;
use App\Traits\ApiResponser;
use App\Traits\HasPermission;
use Auth;
use Illuminate\Support\Facades\Log;

class CatalogController extends Controller
{
    use ApiResponser;
	public $microService;
    use HasPermission;
	public function __construct(Request $request, UssbnbService $microService)
    {
        $this->microService = $microService;
        $this->page_title = $request->route()->getName();
        $description = \Request::route()->getAction();
        $this->page_desc = isset($description['desc']) ? $description['desc'] : $this->page_title;
    }

    public function showCountries()
    {
        $data['page_title'] = $this->page_title;
        $admin_user_id 		= Auth::user()->id;
        $data['module_name']= "";
        $data['sub_module']= "";
        $add_action_id = 5; // Action Management
        $data['actions'] = "";
        $add_permisiion = $this->PermissionHasOrNot($admin_user_id,$add_action_id );
        $actions['add_permisiion']= $add_permisiion;

        return view('catalog.country.list', compact('countries','actions'));
    }

    //------------------------ Apis for Countries----------------------------
    function getCountries(){
        $response_data =  $this->microService->getAllCountries();
        return $response_data;
    }

    public function ajaxCountryList(){
        $admin_user_id 		= Auth::user()->id;
        $edit_action_id 	= 10;
        $delete_action_id 	= 11;

        $edit_permisiion 	= $this->PermissionHasOrNot($admin_user_id,$edit_action_id);
        $delete_permisiion 	= $this->PermissionHasOrNot($admin_user_id,$delete_action_id);

        $countries = $this->getCountries();

        $return_arr = array();
        foreach($countries->data->countries as $country){
            $data['actions'] = "";
            $data['status'] = ($country->status == 1)?"<button class='btn btn-xs btn-success' disabled>Active</button>":"<button class='btn btn-xs btn-success' disabled>In-active</button>";
            $data['id'] = $country->id;
            $data['name'] = $country->name;
            if($edit_permisiion>0){
                $data['actions'] .="<button onclick='countryEdit(".$country->id.")' id=edit_" . $country->id . "  class='btn btn-xs btn-green module-edit' ><i class='clip-pencil-3'></i></button>";
            }
            if ($delete_permisiion>0) {
                $data['actions'] .=" <button onclick='countryDelete(".$country->id.")' id='delete_" . $country->id . "' class='btn btn-xs btn-danger' ><i class='clip-remove'></i></button>";
            }

            $return_arr[] = $data;
        }
        return json_encode(array('data'=>$return_arr));
    }

	//getting parent menu
	public function getCountryList(){
		$response_data = json_decode($this->microService->getAllCountries());
		return json_encode(array('data'=>$response_data->data->countries));
	}


    public function createCountry(Request $request){

        // update
        if(!is_null($request->input('country_id')) && $request->input('country_id') != ""){
            $response_data =  json_encode($this->microService->updateCountry($request->all(), $request->input('country_id')));
        }
        else {
            $response_data =  json_encode($this->microService->createCountry($request->all()));
        }

        return $response_data;
	}

    public function editCountry($id) {
        $response_data =  $this->microService->editCountry($id);
        return json_encode($response_data);
    }

    // delete country
    public function countryDelete ($id) {
        $response_data =  json_decode($this->microService->deleteCountry($id));
        if($response_data->success==true){
            return json_encode(array(
                "deleteMessage"=>"Deleted Successful",
            ));
        }
        else{
            return json_encode(array(
                "parentmessage"=>"Error",
            ));
        }
    }
	//************************************************Locations***********************************************************

	//Get location list
	public function locationManagement(){
		$data['page_title'] 	= $this->page_title;
		$data['module_name']	= "Catalog";
		$data['sub_module']		= "Locations";

		$data['countries'] 		=  $this->getCountries();

		// action permissions
        $admin_user_id  		= Auth::user()->id;
        $add_action_id  		= 30; // Location entry
        $add_permisiion 		= $this->PermissionHasOrNot($admin_user_id,$add_action_id );
        $data['actions']['add_permisiion']= $add_permisiion;

		return view('catalog.locations',$data);
	}

	//Location list by ajax
	public function locationList(){
		$admin_user_id 		= Auth::user()->id;
		$edit_action_id 	= 31; // Location edit
		$delete_action_id 	= 32; // Location delete
		$edit_permisiion 	= $this->PermissionHasOrNot($admin_user_id,$edit_action_id);
		$delete_permisiion 	= $this->PermissionHasOrNot($admin_user_id,$delete_action_id);

        $locations = $this->microService->getAlllocations();

        $return_arr = array();
        foreach($locations->data->locations as $location){
			//var_dump($location);die;
            $data['actions'] = "";
            $data['status'] = ($location->status == 1)?"<button class='btn btn-xs btn-success' disabled>Active</button>":"<button class='btn btn-xs btn-success' disabled>In-active</button>";
            $data['id'] = $location->id;
            $data['location_name'] = $location->location_name;
			$data['country_name'] = $location->country->name;
            if($edit_permisiion>0){
                $data['actions'] .="<button onclick='locationEdit(".$location->id.")' id=edit_" . $location->id . "  class='btn btn-xs btn-green module-edit' ><i class='clip-pencil-3'></i></button>";
            }
            if ($delete_permisiion>0) {
                $data['actions'] .=" <button onclick='locationDelete(".$location->id.")' id='delete_" . $location->id . "' class='btn btn-xs btn-danger' ><i class='clip-remove'></i></button>";
            }

            $return_arr[] = $data;
        }
        return json_encode(array('data'=>$return_arr));
	}


	//Location Entry
	public function locationEntry(Request $request){
		if(!isset($request->status)){
			$request->input('status', 0);
		}
		// update
		if(!is_null($request->input('edit_id')) && $request->input('edit_id') != ""){
			$response_data =  $this->microService->updateLocation($request->all(), $request->input('edit_id') );
		}
		// new entry
		else{
			$response_data =  $this->microService->createLocation($request->all());
		}

        return json_encode($response_data);
	}

	//get data for update
	public function locationEdit($id){
		$location = $this->microService->getLocationById($id);
		return json_encode($location->data);
	}

	public function locationDelete($id) {

	    // delete country
        $response_data =  $this->microService->deleteLocation($id);
        if($response_data->success==true){
            return json_encode(array(
                "deleteMessage"=>"Deleted Successful",
            ));
        }
        else{
            return json_encode(array(
                "parentmessage"=>"Error",
            ));
        }
    }

	//-------------------------------------------- end location ----------------------------------------------------------



//************************************************Buildings***********************************************************

	//Get building list
	public function buildingManagement(){
		$data['page_title'] 	= $this->page_title;
		$data['module_name']	= "Catalog";
		$data['sub_module']		= "Buildings";

		$locations 				= $this->microService->getAlllocations();
		$data['locations'] 		= $locations->data->locations;
		//var_dump($data['locations'] );die;
		// action permissions
        $admin_user_id  		= Auth::user()->id;
        $add_action_id  		= 34 ;// building entry
        $add_permisiion 		= $this->PermissionHasOrNot($admin_user_id,$add_action_id );
        $data['actions']['add_permisiion']= $add_permisiion;

		return view('catalog.buildings',$data);
	}

	//building list by ajax
	public function buildingList(){
		$admin_user_id 		= Auth::user()->id;
		$edit_action_id 	= 35; // building edit
		$delete_action_id 	= 36; // building delete
		$edit_permisiion 	= $this->PermissionHasOrNot($admin_user_id,$edit_action_id);
		$delete_permisiion 	= $this->PermissionHasOrNot($admin_user_id,$delete_action_id);

        $buildings = $this->microService->getAllBuildings();
		//DD($buildings );die;
        $return_arr = array();
        foreach($buildings->data->buildings as $building){
			//var_dump($building);die;
            $data['actions'] 		= "";
            $data['status'] 		= ($building->status == 1)?"<button class='btn btn-xs btn-success' disabled>Active</button>":"<button class='btn btn-xs btn-success' disabled>In-active</button>";
            $data['id'] 			= $building->id;
            $data['building_name'] 	= $building->name;
			$data['address'] 		= $building->address;
			//$data['image'] 		= $building->image;
			$data['location_name'] 	= $building->locations->location_name.', '.$building->locations->country->name;
			//$data['country_name'] 	= $building->locations->country->name;
            if($edit_permisiion>0){
                $data['actions'] .="<button onclick='buildingAddImage(".$building->id.")' id=edit_" . $building->id . "  class='btn btn-xs btn-warning' ><i class='clip-images'></i></button>";
				$data['actions'] .="<button onclick='buildingEdit(".$building->id.")' id=edit_" . $building->id . "  class='btn btn-xs btn-green' ><i class='clip-pencil-3'></i></button>";
            }
            if ($delete_permisiion>0) {
                $data['actions'] .=" <button onclick='buildingDelete(".$building->id.")' id='delete_" . $building->id . "' class='btn btn-xs btn-danger' ><i class='clip-remove'></i></button>";
            }

            $return_arr[] = $data;
        }
        return json_encode(array('data'=>$return_arr));
	}

	//building Entry
	public function buildingEntry(Request $request){

	    if(!isset($request->status)){
			$request->input('status', 0);
		}
		// update
		if(!is_null($request->input('id')) && $request->input('id') != ""){
			$response_data =  $this->microService->updateBuilding($request->all(), $request->input('id') );
		}
		else{
			$response_data =  $this->microService->createBuilding($request->all());
		}

        return json_encode($response_data);
	}

	//get data for update
	public function buildingEdit($id){
		$building = $this->microService->getBuildingDetail($id);
		return json_encode($building);
	}

	// delete building
    public function buildingDelete ($buildingId) {
        $response_data =  $this->microService->deleteBuilding($buildingId);

        if($response_data){
			return json_encode(array(
				"deleteMessage"=>"Delete Successful",
			));
		}
		else{
			return json_encode(array(
				"parentmessage"=>"Not Deleted!!!",
			));
		}
    }

//-------------------------------------------- end building ----------------------------------------------------------




//************************************************Rooms***********************************************************

	//Get room list
	public function roomManagement(){
		$data['page_title'] 	= $this->page_title;
		$data['module_name']	= "Catalog";
		$data['sub_module']		= "Rooms";

		$buildings 				= $this->microService->getAllBuildings();
		$data['buildings'] 		= $buildings->data->buildings;
		//dd($data['buildings']);
		// action permissions
        $admin_user_id  		= Auth::user()->id;
        $add_action_id  		= 22 ;// room entry
        $add_permisiion 		= $this->PermissionHasOrNot($admin_user_id,$add_action_id );
        $data['actions']['add_permisiion']= $add_permisiion;

		return view('catalog.rooms',$data);
	}

	//room list by ajax
	public function roomList(){
		$admin_user_id 		= Auth::user()->id;
		$edit_action_id 	= 23; // room edit
		$delete_action_id 	= 24; // room delete
		$edit_permisiion 	= $this->PermissionHasOrNot($admin_user_id,$edit_action_id);
		$delete_permisiion 	= $this->PermissionHasOrNot($admin_user_id,$delete_action_id);

        $rooms = $this->microService->getAllrooms();
		//DD($rooms );die;
        $return_arr = array();
        foreach($rooms->data->rooms as $room){

            $data['actions'] 		= "";
            $data['status'] 		= ($room->status == 1)?"<button class='btn btn-xs btn-success' disabled>Active</button>":"<button class='btn btn-xs btn-success' disabled>In-active</button>";
            $data['id'] 			= $room->id;
            $data['name'] 			= $room->name;
			$data['rate'] 			= $room->price_per_hour;
			$data['max_person'] 	= $room->maximum_person;
			$buildings 				= $this->microService->getBuildingDetail($room->building_id);
            $buildings = $buildings->data->buildings;
			$data['location_name'] 	= $buildings->locations->location_name.', '.$buildings->locations->country->name;
			$data['building_name'] 	= $buildings->name;

            $data['actions'] .=" <button title='View' onclick='roomView(".$room->id.")' id='view_" . $room->id . "' class='btn btn-xs btn-primary admin-user-view' ><i class='clip-zoom-in'></i></button>&nbsp;";
			if($edit_permisiion>0){
                $data['actions'] .="<button onclick='roomAddImage(".$room->id.")' id=edit_" . $room->id . "  class='btn btn-xs btn-warning' ><i class='clip-images'></i></button>&nbsp;";
				$data['actions'] .="<button onclick='roomEdit(".$room->id.")' id=edit_" . $room->id . "  class='btn btn-xs btn-green' ><i class='clip-pencil-3'></i></button>";
            }
            if ($delete_permisiion>0) {
                $data['actions'] .=" <button onclick='roomDelete(".$room->id.")' id='delete_" . $room->id . "' class='btn btn-xs btn-danger' ><i class='clip-remove'></i></button>";
            }

            $return_arr[] = $data;
        }
        return json_encode(array('data'=>$return_arr));
	}

	//room Entry
	public function roomEntry(Request $request){

	    if(!isset($request->status)){
			$request->input('status', 0);
		}
		// update
		if(!is_null($request->input('id')) && $request->input('id') != ""){
			$response_data =  $this->microService->updateRoom($request->all(), $request->input('id') );
		}
		// new entry
		else{
			Log::debug("room", array(0=>$request->all()));
			$response_data =  $this->microService->createRoom($request->all());
		}

        return json_encode($response_data);
	}

	//get data for update
	public function roomEdit($id){
		$room = $this->microService->getRoomDetail($id);

		return json_encode($room->data);
	}

	//get data for update
	public function roomShow($id){
		$room = $this->microService->getRoomDetail($id);
		//dd($room);
		return json_encode($room->data);
	}

	// delete room
    public function roomDelete ($roomId) {
        $response_data =  $this->microService->deleteRoom($roomId);
		if($response_data->success){
			return json_encode(array(
				"deleteMessage"=>"Delete Successful",
			));
		}
		else{
			return json_encode(array(
				"parentmessage"=>"Not Deleted!!!",
			));
		}
    }

//-------------------------------------------- end room ----------------------------------------------------------



}
