<?php
namespace App\Services;

use Apiz\AbstractApi;
use Illuminate\Support\Facades\Log;

class UssbnbService extends AbstractApi
{
    protected function setBaseUrl() {
        $url = config('app.url');
        Log::debug("url", array(0=>"$url:8009"));

        return "$url:8009";
    }

    protected function setPrefix () {
        return 'api/v1';
    }

    public function getAllCountries() {
      $countries = $this->get("/countries");
      $data = json_decode($countries->getContents());

      if ($countries->getStatusCode() == 200 || $countries->getStatusCode() == 202) {
        return $data;
      }

      return null;
    }

  function createCountry($data){
      $country = $this->formParams($data)->post("/countries");
      $data = json_decode($country->getContents());

      if ($country->getStatusCode() == 200 || $country->getStatusCode() == 201) {
          return $data;
      }
      return null;
  }

  function editCountry($id){
      $countries = $this->get("/countries/$id");
      $data = json_decode($countries->getContents());

      if ($countries->getStatusCode() == 200 || $countries->getStatusCode() == 202) {
          return $data;
      }

      return null;
  }

  function updateCountry($data, $id) {
      $countries = $this->formParams($data)->put("/countries/$id", $data);

      $data = json_decode($countries->getContents());
      if ($countries->getStatusCode() == 200 || $countries->getStatusCode() == 201) {
          return $data;
      }

      return null;
  }

  function deleteCountry($id) {
      $countries = $this->delete("/countries/$id");
      $data = json_decode($countries->getContents());

      if ($countries->getStatusCode() == 200 || $countries->getStatusCode() == 202) {
          return $data;
      }

      return null;
  }

  //-------------------------- location------------------------------------

   public function getAlllocations() {
       $locations = $this->get("/locations");

       $data = json_decode($locations->getContents());

       if ($locations->getStatusCode() == 200 || $locations->getStatusCode() == 202) {
           return $data;
       }

       return null;
   }
    public function createLocation($data){
        $location = $this->formParams($data)->post("/locations");
        $data = json_decode($location->getContents());

        if ($location->getStatusCode() == 200 || $location->getStatusCode() == 201) {
            return $data;
        }
        return null;
    }
  public function getLocationById($locationId) {

      $location = $this->get("/locations/$locationId");
      $data = json_decode($location->getContents());

      if ($location->getStatusCode() == 200 || $location->getStatusCode() == 202) {
          return $data;
      }

      return null;
  }
  public function updateLocation($data, $locationId) {
      $location = $this->formParams($data)->put("/locations/$locationId", $data);

      $data = json_decode($location->getContents());
      if ($location->getStatusCode() == 200 || $location->getStatusCode() == 201) {
          return $data;
      }

      return null;
  }
    public function deleteLocation($locationId) {
        $location = $this->delete("/locations/$locationId");
        $data = json_decode($location->getContents());

        if ($location->getStatusCode() == 200 || $location->getStatusCode() == 202) {
            return $data;
        }

        return null;
  }
    //-----------------------end location ---------------------------------


    //-------------------------- Building------------------------------------
    public function getAllBuildings() {
        $building = $this->get("/buildings");

        $data = json_decode($building->getContents());

        if ($building->getStatusCode() == 200 || $building->getStatusCode() == 202) {
            return $data;
        }

        return null;
  }

  public function getBuildingDetail($id){
      $building = $this->get("/buildings/$id");
      $data = json_decode($building->getContents());

      if ($building->getStatusCode() == 200 || $building->getStatusCode() == 202) {
          return $data;
      }

      return null;
    }

    public function createBuilding($data){

        $building = $this->formParams($data)->post("/buildings");
        $data = json_decode($building->getContents());

        if ($building->getStatusCode() == 200 || $building->getStatusCode() == 201) {
            return $data;
        }
        return null;
  }

  public function updateBuilding($data, $buildingId) {
      $building = $this->formParams($data)->put("/buildings/$buildingId", $data);
      $data = json_decode($building->getContents());

      if ($building->getStatusCode() == 200 || $building->getStatusCode() == 201) {
          return $data;
      }

      return null;
  }
    public function deleteBuilding($buildingId) {
        $building = $this->delete("/buildings/$buildingId");
        $data = json_decode($building->getContents());

        if ($building->getStatusCode() == 200 || $building->getStatusCode() == 201) {
            return $data;
        }

        return null;
    }

//--------------------------- Rooms functions--------------------------
    function getAllRooms(){

        $rooms = $this->get("/rooms");
        
        $data = json_decode($rooms->getContents());

        if ($rooms->getStatusCode() == 200 || $rooms->getStatusCode() == 202) {
            return $data;
        }

        return null;
    }

    function createRoom($data){

        $rooms = $this->formParams($data)->post("/rooms");
        $data = json_decode($rooms->getContents());
        Log::debug("room", array(0=>$data));    
        if ($rooms->getStatusCode() == 200 || $rooms->getStatusCode() == 201) {
            return $data;
        }
        return null;
    }

    function getRoomDetail($id){
        $room = $this->get("/rooms/$id");
        $data = json_decode($room->getContents());

        if ($room->getStatusCode() == 200 || $room->getStatusCode() == 202) {
            return $data;
        }

        return null;
    }
    function getEditRoomDetail($id){
      return $this->performRequest('GET', "/api/v1/rooms/$id/edit");
    }

    function updateRoom($data, $id){
        $room = $this->formParams($data)->put("/rooms/$id", $data);
        $data = json_decode($room->getContents());

        if ($room->getStatusCode() == 200 || $room->getStatusCode() == 201) {
            return $data;
        }

        return null;
    }

    function deleteRoom($id){
        $room = $this->delete("/rooms/$id");
        $data = json_decode($room->getContents());

        if ($room->getStatusCode() == 200 || $room->getStatusCode() == 201) {
            return $data;
        }

        return null;
    }
//------------------------------end Rooms---------------------------------




//--------------------------- Booking functions--------------------------
    function getAllBookings(){
        
        $bookings = $this->get("/bookings");

        $data = json_decode($bookings->getContents());
        
        if ($bookings->getStatusCode() == 200 || $bookings->getStatusCode() == 202) {
            return $data;
        }

        return null;
    }

  function createBooking($data){
      $bookings = $this->formParams($data)->post("/bookings");
      $data = json_decode($bookings->getContents());
      
      if ($bookings->getStatusCode() == 200 || $bookings->getStatusCode() == 201) {
          return $data;
      }
      return null;
  }

  function getBookingDetail($id){
      $bookings = $this->get("/bookings/$id");
      $data = json_decode($bookings->getContents());

      if ($bookings->getStatusCode() == 200 || $bookings->getStatusCode() == 202) {
          return $data;
      }

      return null;
  }

  function getEditBookingDetail($id) {
      return $this->performRequest('GET', "/api/v1/booking/edit/$id");
  }

  function updateBooking($data, $id) {
      $bookings = $this->formParams($data)->put("/bookings/$id", $data);
      $data = json_decode($bookings->getContents());

      if ($bookings->getStatusCode() == 200 || $bookings->getStatusCode() == 201) {
          return $data;
      }

      return null;
  }

  function updateBookingStatus($status, $id) {
      return $this->performRequest('GET', "/api/v1/booking/status/$status/$id");
  }

  function deleteBooking($id)
  {
      $booking = $this->delete("/bookings/$id");
      $data = json_decode($booking->getContents());

      if ($booking->getStatusCode() == 200 || $booking->getStatusCode() == 201) {
          return $data;
      }

      return null;
  }

//------------------------------end Booking---------------------------------

}
