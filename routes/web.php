<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('buildings',array('as'=>'Sign in', 'uses' =>'CatalogController@buildings'));


#Login

Route::get('/',array('as'=>'Sign in', 'uses' =>'AuthController@authLogin'));
Route::get('/login',array('as'=>'Sign in', 'uses' =>'AuthController@authLogin'));
Route::get('/auth',array('as'=>'Sign in', 'uses' =>'AuthController@authLogin'));
Route::get('auth/login',array('as'=>'Sign in', 'uses' =>'AuthController@authLogin'));
Route::post('auth/post/login',array('as'=>'Sign in', 'uses' =>'AuthController@authPostLogin'));


#ForgetPassword
Route::get('auth/forget/password',array('as'=>'Forgot Password' , 'uses' =>'AuthController@forgetPasswordAuthPage'));
Route::post('auth/forget/password',array('as'=>'Forgot Password' , 'uses' =>'AuthController@authForgotPasswordConfirm'));
Route::get('auth/forget/password/{user_id}/verify',array('as'=>'Forgot Password Verify' , 'uses' =>'AuthController@authSystemForgotPasswordVerification'));
Route::post('auth/forget/password/{user_id}/verify',array('as'=>'New Password Submit' , 'uses' =>'AuthController@authSystemNewPasswordPost'));

// need only authentication
Route::group(['middleware' => ['auth']], function () {
	Route::get('/',array('as'=>'Dashboard' , 'uses' =>'AdminController@index'));
    Route::get('auth/logout/{email}',array('as'=>'Logout' , 'uses' =>'AuthController@authLogout'));
	Route::get('/dashboard',array('as'=>'Dashboard' , 'uses' =>'AdminController@index'));

	//my Profile
	Route::get('/profile/my-profile',array('as'=>'My Profile Management', 		'uses' =>'AdminController@profileIndex'));
	Route::get('/profile/my-profile-info',array('as'=>'Get My Profile Info', 	'uses' =>'AdminController@profileInfo'));
	Route::post('/profile/my-profile-update',array('as'=>'Update My Profile Info', 'uses' =>'AdminController@updateProfile'));
	Route::post('/profile/password-update',array('as'=>'Update My Profile Info', 'uses' =>'AdminController@updatePassword'));


	//Menus/ Modules
	Route::get('/module/get-parent-menu',array('as'=>'Parent Menu List' ,'uses' =>'SettingController@getParentMenu'));
	Route::get('/web-action/get-module-name',array('as'=>'Web Action Management' , 'uses' =>'SettingController@getModuleName'));

	Route::get('/admin/load-actions-for-group-permission/{id}',array('as'=>'Load Actions', 'uses' =>'AdminController@load_actions_for_group_permission'));

	Route::get('/country/country-list',array('as'=>'Country List' ,'uses' =>'CatalogController@getCountryList'));


});

// need  authentication and permission to access the action/action-lists
// you have to define the action no from DB
Route::group([/*'middleware' => ['auth','permission']*/ ], function () {

	//Admin User
	Route::get('user/admin/admin-user-management',array('as'=>'Admin Users' , 'action_id'=>'1', 'uses' =>'AdminController@adminUserManagement'));
	Route::get('/admin/ajax/admin-list',array('as'=>'Admin User List' ,  'action_id'=>'1','uses' =>'AdminController@ajaxAdminList'));
	Route::get('/admin/admin-view/{id}',array('as'=>'Admin View' , 'action_id'=>'1', 'uses' =>'AdminController@adminUserView'));
	Route::post('/admin/admin-user-entry',array('as'=>'Admin User Entry', 'action_id'=>'2', 'uses' =>'AdminController@ajaxAdminEntry'));
	Route::get('/admin/edit/{id}',array('as'=>'Admin Edit', 'action_id'=>'3', 'uses' =>'AdminController@adminUserEdit'));
	Route::get('/admin/delete/{id}',array('as'=>'Admin Delete', 'action_id'=>'4', 'uses' =>'AdminController@adminDestroy'));

	// Actions
	Route::get('cp/web-action/web-action-management',array('as'=>'Web Action Management', 'action_id'=>'5', 'uses' =>'SettingController@webActionManagement'));
	Route::get('/web-action/action-lists',array('as'=>'Web Action List', 'action_id'=>'5' , 'uses' =>'SettingController@webActionList'));
	Route::post('/web-action/web-action-entry',array('as'=>'Web Action Entry', 'action_id'=>'6', 'uses' =>'SettingController@webActionEntry'));
	Route::get('/web-action/edit/{id}',array('as'=>'Web Action Edit', 'action_id'=>'7', 'uses' =>'SettingController@web_action_edit'));

	//Menus/ Modules
	Route::get('cp/module/manage-module',array('as'=>'Manage Module' , 'action_id'=>'8', 'uses' =>'SettingController@moduleManagement'));
	Route::get('/module/menu-list',array('as'=>'Menu List' ,'action_id'=>'8', 'uses' =>'SettingController@ajaxMenuList'));
	Route::post('/module/module-entry/',array('as'=>'Module Entry' , 'action_id'=>'9', 'uses' =>'SettingController@moduleEntry'));
	Route::get('/module/edit/{id}',array('as'=>'Module Edit' , 'action_id'=>'10', 'uses' =>'SettingController@moduleEdit'));
	Route::get('/module/delete/{id}',array('as'=>'Module Edit' , 'action_id'=>'11', 'uses' =>'SettingController@moduleDelete'));

	//General Setting
	Route::get('settings/general/general-setting',array('as'=>'General Setting Management', 'action_id'=>'12', 'uses' =>'SettingController@generalSetting'));
	Route::post('/general/setting-update',array('as'=>'General Setting Update', 'action_id'=>'15', 'uses' =>'SettingController@generalSettingUpdate'));


	//Admin User Group
	Route::get('settings/admin/admin-group-management',array('as'=>'Admin User Groups Management', 'action_id'=>'16', 'uses' =>'AdminController@admin_user_groups'));
	Route::get('/admin/admin-group-list',array('as'=>'Admin Groups List' ,   'action_id'=>'16','uses' =>'AdminController@admin_groups_list'));
	Route::post('/admin/admin-group-entry',array('as'=>'Admin Groups Entry', 'action_id'=>'17', 'uses' =>'AdminController@admin_groups_entry_or_update'));
	Route::get('/admin/admin-group-edit/{id}',array('as'=>'Admin Groups Edit', 'action_id'=>'18', 'uses' =>'AdminController@admin_group_edit'));
	Route::get('/admin/admin-group-delete/{id}',array('as'=>'Admin Groups Delete', 'action_id'=>'19', 'uses' =>'AdminController@admin_group_delete'));
	//Permission
	Route::post('/admin/permission-action-entry-update',array('as'=>'Permission Entry', 'action_id'=>'20', 'uses' =>'AdminController@permission_action_entry_update'));


	//catalog
    Route::get('/admin/country/listing',array('as'=>'View Country Listing', 'action_id'=>'23', 'uses' =>'CatalogController@showCountries'));
    Route::get('/ajax/country/listing',array('as'=>'Catalog Country Listing', 'action_id'=>'23', 'uses' =>'CatalogController@ajaxCountryList'));
    Route::post('/ajax/country/store',array('as'=>'Catalog Country Listing', 'action_id'=>'23', 'uses' =>'CatalogController@createCountry'));
    Route::get('/ajax/country/edit/{id}',array('as'=>'Catalog Country Editing', 'action_id'=>'23', 'uses' =>'CatalogController@editCountry'));
    Route::get('/ajax/country/delete/{id}',array('as'=>'Catalog Country Editing', 'action_id'=>'23', 'uses' =>'CatalogController@countryDelete'));

	Route::get('/locations/list',array('as'=>'Location Listing', 'action_id'=>'29', 'uses' =>'CatalogController@locationManagement'));
	Route::get('/locations/location-list',array('as'=>'Location List' ,'action_id'=>'29', 'uses' =>'CatalogController@locationList'));
	Route::post('/location/location-entry',array('as'=>'Location Entry' , 'action_id'=>'30', 'uses' =>'CatalogController@locationEntry'));
	Route::get('/location/edit/{id}',array('as'=>'Location Edit' , 'action_id'=>'31', 'uses' =>'CatalogController@locationEdit'));
	Route::get('/location/delete/{id}',array('as'=>'Location Delete' , 'action_id'=>'32', 'uses' =>'CatalogController@locationDelete'));

  	Route::get('/buildings/list',array('as'=>'building Listing', 'action_id'=>'33', 'uses' =>'CatalogController@buildingManagement'));
	Route::get('/building/building-list',array('as'=>'building List' ,'action_id'=>'33', 'uses' =>'CatalogController@buildingList'));
	Route::post('/building/building-entry',array('as'=>'building Entry' , 'action_id'=>'34', 'uses' =>'CatalogController@buildingEntry'));
	Route::get('/building/edit/{id}',array('as'=>'building Edit' , 'action_id'=>'35', 'uses' =>'CatalogController@buildingEdit'));
	Route::get('/building/delete/{id}',array('as'=>'building Delete' , 'action_id'=>'36', 'uses' =>'CatalogController@buildingDelete'));


  	Route::get('/rooms/list',array('as'=>'Room Listing', 'action_id'=>'21', 'uses' =>'CatalogController@roomManagement'));
	Route::get('/room/room-list',array('as'=>'Room List' ,'action_id'=>'21', 'uses' =>'CatalogController@roomList'));
	Route::get('/room/room-view/{id}',array('as'=>'Room Details View' ,'action_id'=>'21', 'uses' =>'CatalogController@roomShow'));
	Route::post('/room/room-entry',array('as'=>'Room Entry' , 'action_id'=>'22', 'uses' =>'CatalogController@roomEntry'));
	Route::get('/room/edit/{id}',array('as'=>'Room Edit' , 'action_id'=>'23', 'uses' =>'CatalogController@roomEdit'));
	Route::get('/room/delete/{id}',array('as'=>'Room Delete' , 'action_id'=>'24', 'uses' =>'CatalogController@roomDelete'));

	Route::get('/booking/list',array('as'=>'Bookin Listing', 'action_id'=>'37', 'uses' =>'BookingController@bookingManagement'));
	Route::get('/booking/booking-list',array('as'=>'Bookin List' ,'action_id'=>'37', 'uses' =>'BookingController@bookingList'));

	Route::get('/booking/view/{id}',array('as'=>'Bookin Details View' ,'action_id'=>'37', 'uses' =>'BookingController@bookingShow'));
	Route::post('/booking/booking-entry',array('as'=>'Bookin Entry' , 'action_id'=>'38', 'uses' =>'BookingController@bookingEntry'))->name('booking.create');
	Route::get('/booking/edit/{id}',array('as'=>'Bookin Edit' , 'action_id'=>'39', 'uses' =>'BookingController@bookingEdit'));
	Route::get('/booking/delete/{id}',array('as'=>'Bookin Delete' , 'action_id'=>'40', 'uses' =>'BookingController@bookingDelete'));
	Route::get('/booking/status/{status}/{id}',array('as'=>'Bookin Status Update' , 'action_id'=>'40', 'uses' =>'BookingController@updateBookingStatus'));


});

