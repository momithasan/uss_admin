@extends('layout.master')
@section('content')
    <!--MESSAGE-->
    <div class="row ">
        <div class="col-md-10 col-md-offset-1" id="master_message_div">
        </div>
    </div>
    <!--END MESSAGE-->

    <!--PAGE CONTENT -->
    <div class="row ">
        <div class="col-sm-12">
            <div class="tabbable">
                <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">
                    <li class="active">
                        <a id="country_add" data-toggle="tab" href="#country_list">
                            <b> Country List</b>
                        </a>
                    </li>
                    @if($actions['add_permisiion'] > 0)
                        <li class="">
                            <a data-toggle="tab" href="#entry_form_div" id="country_add_button">
                                <b> Add Country</b>
                            </a>
                        </li>
                    @endif
                </ul>
                <div class="tab-content">
                    <!-- PANEL FOR OVERVIEW-->
                    <div id="user_list_div" class="tab-pane in active">
                        <div class="row no-margin-row">
                            <!-- List of Categories -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-external-link-square"></i>

                                    <div class="panel-tools">
                                        <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                        </a>
                                        <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
                                            <i class="fa fa-wrench"></i>
                                        </a>
                                        <a class="btn btn-xs btn-link panel-refresh" href="#">
                                            <i class="fa fa-refresh"></i>
                                        </a>
                                        <a class="btn btn-xs btn-link panel-expand" href="#">
                                            <i class="fa fa-resize-full"></i>
                                        </a>
                                        <a class="btn btn-xs btn-link panel-close" href="#">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-bordered table-hover country" id="country" style="width:100% !important">
                                        <thead>
                                            <tr>
                                                <th width="10%">ID</th>

                                                <th width="50%">Name</th>
                                                <th width="20%">Status</th>
                                                <th width="20%">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END Categoreis -->
                        </div>
                    </div>
                    <!--END PANEL FOR OVERVIEW -->

                    <!-- PANEL FOR CHANGE PASSWORD -->
                    <div id="entry_form_div" class="tab-pane in">
                        <div class="row no-margin-row">
                            <form id="country" autocomplete="off" name="admin_user_form" enctype="multipart/form-data" class="countryForm form-horizontal form-label-left">
                                @csrf
                                <input type="hidden" name="country_id" id="country_id" value="">
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <label class="control-label col-md-2 col-sm-2 col-xs-6">Country Name<span class="required">*</span></label>
                                            <div class="col-md-5 col-sm-4 col-xs-6">
                                                <input type="text" id="name" name="name" required class="form-control col-lg-12"/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-2 col-sm-2 col-xs-6" >Is Active</label>
                                            <div class="col-md-1 col-sm-1 col-xs-6">
                                                <input type="checkbox" id="is_active" name="is_active" checked="checked" value="1" class="form-control col-lg-12"/>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" name="id" id="id">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-6"></label>
                                    <div class="col-md-4 col-sm-4 col-xs-12">


                                        <button type="submit" id="add_country" class="btn btn-success add_country">Save</button>
                                        <button type="button" id="clear_button" class="btn btn-warning">Clear</button>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div id="form_submit_error" class="text-center" style="display:none"></div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END PANEL FOR CHANGE PASSWORD -->
                </div>
            </div>
        </div>
    </div>
    </div>
    <!--END PAGE CONTENT-->
@endsection
@section('JScript')
    <script>
        var profile_image_url = "<?php echo asset('assets/images/user/admin'); ?>";
    </script>
    <script src="{{ asset('assets/js/ussbnb/catalog/country.js')}}"></script>
@endsection


