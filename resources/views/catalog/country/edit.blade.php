@extends('layout.master')
@section('content')
    <!--MESSAGE-->
    <div class="row ">
        <div class="col-md-10 col-md-offset-1" id="master_message_div">
        </div>
    </div>
    <!--END MESSAGE-->

    <!--PAGE CONTENT -->
    <div class="row ">
        <div class="col-sm-12">

                    <div id="entry_form_div" class="tab-pane in">
                        <div class="row no-margin-row">
                            <form id="update_country" autocomplete="off" enctype="multipart/form-data" class="countryForm form-horizontal form-label-left" method="post">
                                @csrf
                                @method('put')

                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <label class="control-label col-md-2 col-sm-2 col-xs-6">Country Name<span class="required">*</span></label>
                                            <div class="col-md-5 col-sm-4 col-xs-6">
                                                <input value="{{ $country[0]->name }}" type="text" id="name" name="name" required class="form-control col-lg-12"/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-2 col-sm-2 col-xs-6" >Is Active</label>
                                            <div class="col-md-1 col-sm-1 col-xs-6">
                                                <input value="1" {{$country[0]->status ? "checked" : ''  }}
                                                    type="checkbox" id="is_active" name="is_active"   class="form-control col-lg-12"/>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <input type="hidden" name="id" value="{{$country[0]->id}}">
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-6"></label>
                                    <div class="col-md-4 col-sm-4 col-xs-12">


                                        <button type="submit" id="update_country" class="btn btn-success update_country">Save</button>
                                        <button type="button" id="clear_button" class="btn btn-warning">Clear</button>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div id="form_submit_error" class="text-center" style="display:none"></div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END PANEL FOR CHANGE PASSWORD -->

        </div>
    </div>
    <!--END PAGE CONTENT-->
@endsection
@section('JScript')
    <script>
        var profile_image_url = "<?php echo asset('assets/images/user/admin'); ?>";
    </script>
    <script src="{{ asset('assets/js/ussbnb/catalog/country.js')}}"></script>
@endsection


