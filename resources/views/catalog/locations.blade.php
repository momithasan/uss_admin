@extends('layout.master')
@section('content')

    <!--PAGE CONTENT -->
    <div class="row ">
        <div class="col-sm-12">            
        	<div class="col-md-6 col-sm-12">
        		<h4 class="text-info">Manage Locations</h4><hr>
        		<div id="entry_form_div" class="tab-pane in">
					<div class="row no-margin-row">
						<form id="location_form" name="location_form" enctype="multipart/form-data" class="form form-horizontal form-label-left">
							@csrf
							<input type="hidden" name="edit_id" id="edit_id">
							<div class="row">
							<div class="col-md-12">
								<div class="form-group"> 
									<label class="control-label col-md-3 col-sm-3 col-xs-6" >Location Name</label>
									<div class="col-md-9 col-sm-9  col-xs-6">
										<input type="text" id="location_name" name="location_name" required class="form-control col-lg-12"/>
									</div>
								</div>
								
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-6" >Country</label>
									<!--<div class="col-md-9 col-sm-9  col-xs-6">
										<input type="text" id="country_name" name="country_name" class="form-control col-lg-12" list="country_list" />
										<datalist id="country_list" class="dropdown">
											
										</datalist>
									</div>
									
									<div class="col-md-9 col-sm-9  col-xs-6">
										<input type="text" id="country_name" name="country_name" class="form-control col-lg-12"/>
										<input type="hidden" name="country_id" id="country_id">
									</div>
									
									-->
									<div class="col-md-9 col-sm-9  col-xs-6">
										<select class="form-control col-lg-12" id="country_id"  name="country_id">
											<option value="0"  selected>Select Country</option>
											@foreach($countries->data->countries as $country){
												<option value="{{$country->id}}">{{$country->name}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-6" >Is Active</label>
									<div class="col-md-4 col-sm-4 col-xs-6">
										<input type="checkbox" id="status" name="status" checked="checked" value="1" class="form-control col-lg-12"/>
									</div>
								</div>
								<div class="ln_solid"></div>
							</div>
							
							</div>
							<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-6"></label>
							<div class="col-md-4 col-sm-4 col-xs-12">
								@if($actions['add_permisiion']>0)
									<button type="submit" id="save_location" class="btn btn-success">Save</button>                    
									<button type="button" id="clear_button" class="btn btn-warning">Clear</button>
								@endif                         
							</div>
							 <div class="col-md-6 col-sm-6 col-xs-12">
								<div id="form_submit_error" class="text-center" style="display:none"></div>
							 </div>
						</div>
						</form>		
					</div>
				</div>
        	</div>
			<div class="col-md-6 col-sm-12">
				<div id="user_list_div" class="tab-pane in active">
					<div class="row no-margin-row">
		                <!-- List of Categories -->
						<div class="panel panel-default">
							<div class="panel-heading">
								<i class="fa fa-external-link-square"></i>

								<div class="panel-tools">
									<a class="btn btn-xs btn-link panel-collapse collapses" href="#"></a>
									<a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
										<i class="fa fa-wrench"></i>
									</a>
									<a class="btn btn-xs btn-link panel-refresh" href="#">
										<i class="fa fa-refresh"></i>
									</a>
									<a class="btn btn-xs btn-link panel-expand" href="#">
										<i class="fa fa-resize-full"></i>
									</a>
									<a class="btn btn-xs btn-link panel-close" href="#">
										<i class="fa fa-times"></i>
									</a>
								</div>
							</div>
							<div class="panel-body">
								<table class="table table-bordered table-hover locations_table" id="locations_table" style="width:100% !important"> 
									<thead>
										<tr>
											<th> ID</th>
											<th>Location Name</th>
											<th>Country </th>
											<th class="hidden-xs">Status</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
						<!-- END Categoreis -->
		            </div>
		        </div>
			</div>


        </div>
    </div>
    <!--END PAGE CONTENT-->
@endsection


@section('JScript')
	<script type="text/javascript" src="{{ asset('assets/js/ussbnb/catalog/location.js')}}"></script>
@endsection


